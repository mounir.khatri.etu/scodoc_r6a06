# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# ScoDoc
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""
Formulaires configuration Exports Apogée (codes)
"""
import time

from flask import flash, url_for, redirect, request, render_template
from flask_wtf import FlaskForm
from wtforms import BooleanField, SelectField, StringField, SubmitField
from wtforms.validators import Email, Optional
import app
from app.models import ScoDocSiteConfig
import app.scodoc.sco_utils as scu


class BonusConfigurationForm(FlaskForm):
    "Panneau de configuration des logos"
    bonus_sport_func_name = SelectField(
        label="Fonction de calcul des bonus sport&culture",
        choices=[
            (name, displayed_name if name else "Aucune")
            for (name, displayed_name) in ScoDocSiteConfig.get_bonus_sport_class_list()
        ],
    )
    submit_bonus = SubmitField("Valider")
    cancel_bonus = SubmitField("Annuler", render_kw={"formnovalidate": True})


class ScoDocConfigurationForm(FlaskForm):
    "Panneau de configuration avancée"
    enable_entreprises = BooleanField("activer le module <em>entreprises</em>")
    month_debut_annee_scolaire = SelectField(
        label="Mois de début des années scolaires",
        description="""Date pivot. En France métropolitaine, août.
        S'applique à tous les départements.""",
        choices=[
            (i, name.capitalize()) for (i, name) in enumerate(scu.MONTH_NAMES, start=1)
        ],
    )
    month_debut_periode2 = SelectField(
        label="Mois de début deuxième période de l'année",
        description="""Date pivot. En France métropolitaine, décembre.
        S'applique à tous les départements.""",
        choices=[
            (i, name.capitalize()) for (i, name) in enumerate(scu.MONTH_NAMES, start=1)
        ],
    )
    email_from_addr = StringField(
        label="Adresse source des mails",
        description="""adresse email source (from) des mails émis par ScoDoc.
            Attention: si ce champ peut aussi être défini dans chaque département.""",
        validators=[Optional(), Email()],
    )
    user_require_email_institutionnel = BooleanField(
        "imposer la saisie du mail institutionnel dans le formulaire de création utilisateur"
    )
    disable_bul_pdf = BooleanField(
        "interdire les exports des bulletins en PDF (déconseillé)"
    )
    submit_scodoc = SubmitField("Valider")
    cancel_scodoc = SubmitField("Annuler", render_kw={"formnovalidate": True})


def configuration():
    "Page de configuration principale"
    # nb: le contrôle d'accès (SuperAdmin) doit être fait dans la vue
    form_bonus = BonusConfigurationForm(
        data={
            "bonus_sport_func_name": ScoDocSiteConfig.get_bonus_sport_class_name(),
        }
    )
    form_scodoc = ScoDocConfigurationForm(
        data={
            "enable_entreprises": ScoDocSiteConfig.is_entreprises_enabled(),
            "month_debut_annee_scolaire": ScoDocSiteConfig.get_month_debut_annee_scolaire(),
            "month_debut_periode2": ScoDocSiteConfig.get_month_debut_periode2(),
            "email_from_addr": ScoDocSiteConfig.get("email_from_addr"),
            "disable_bul_pdf": ScoDocSiteConfig.is_bul_pdf_disabled(),
            "user_require_email_institutionnel": ScoDocSiteConfig.is_user_require_email_institutionnel_enabled(),
        }
    )
    if request.method == "POST" and (
        form_bonus.cancel_bonus.data or form_scodoc.cancel_scodoc.data
    ):  # cancel button
        return redirect(url_for("scodoc.index"))
    if form_bonus.submit_bonus.data and form_bonus.validate():
        if (
            form_bonus.data["bonus_sport_func_name"]
            != ScoDocSiteConfig.get_bonus_sport_class_name()
        ):
            ScoDocSiteConfig.set_bonus_sport_class(
                form_bonus.data["bonus_sport_func_name"]
            )
            app.clear_scodoc_cache()
            flash("""Fonction bonus sport&culture configurée.""")
        else:
            flash("Fonction bonus inchangée.")
        return redirect(url_for("scodoc.index"))
    elif form_scodoc.submit_scodoc.data and form_scodoc.validate():
        if ScoDocSiteConfig.enable_entreprises(
            enabled=form_scodoc.data["enable_entreprises"]
        ):
            flash(
                "Module entreprise "
                + ("activé" if form_scodoc.data["enable_entreprises"] else "désactivé")
            )
        if ScoDocSiteConfig.set_month_debut_annee_scolaire(
            int(form_scodoc.data["month_debut_annee_scolaire"])
        ):
            flash(
                f"""Début des années scolaires fixé au mois de {
                scu.MONTH_NAMES[ScoDocSiteConfig.get_month_debut_annee_scolaire()-1]
                }"""
            )
        if ScoDocSiteConfig.set_month_debut_periode2(
            int(form_scodoc.data["month_debut_periode2"])
        ):
            flash(
                f"""Début des années scolaires fixé au mois de {
                scu.MONTH_NAMES[ScoDocSiteConfig.get_month_debut_periode2()-1]
                }"""
            )
        if ScoDocSiteConfig.set("email_from_addr", form_scodoc.data["email_from_addr"]):
            flash("Adresse email origine enregistrée")
        if ScoDocSiteConfig.disable_bul_pdf(
            enabled=form_scodoc.data["disable_bul_pdf"]
        ):
            flash(
                "Exports PDF "
                + ("désactivés" if form_scodoc.data["disable_bul_pdf"] else "réactivés")
            )
        if ScoDocSiteConfig.set(
            "user_require_email_institutionnel",
            "on" if form_scodoc.data["user_require_email_institutionnel"] else "",
        ):
            flash(
                (
                    "impose"
                    if form_scodoc.data["user_require_email_institutionnel"]
                    else "n'impose pas"
                )
                + " la saisie du mail institutionnel des utilisateurs"
            )
        return redirect(url_for("scodoc.index"))

    return render_template(
        "configuration.j2",
        form_bonus=form_bonus,
        form_scodoc=form_scodoc,
        scu=scu,
        time=time,
        title="Configuration",
    )
