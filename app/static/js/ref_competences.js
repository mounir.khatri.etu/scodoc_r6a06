class ref_competences extends HTMLElement {
  constructor() {
    super();
    this.shadow = this.attachShadow({ mode: "open" });

    /* Template de base */
    this.shadow.innerHTML = `
			<div class=titre>Cliquer sur un parcours pour afficher ses niveaux de compétences</div>
			<div class=parcours></div>
			<div class=competences></div>
			<div class=ACs></div>
		`;

    /* Style du module */
    const styles = document.createElement("link");
    styles.setAttribute("rel", "stylesheet");
    styles.setAttribute(
      "href",
      removeLastTwoComponents(getCurrentScriptPath()) +
        "/css/ref-competences.css"
    );

    this.shadow.appendChild(styles);
  }

  set setData(data) {
    this.data = data;
    this.parcours();
  }

  parcours() {
    let parcoursDIV = this.shadow.querySelector(".parcours");
    Object.entries(this.data.parcours).forEach(([cle, parcours]) => {
      let div = document.createElement("div");
      div.innerHTML = `<a title="${parcours.libelle}">${parcours.code}</a>`;
      div.addEventListener("click", (event) => {
        this.competences(event, cle);
      });
      parcoursDIV.appendChild(div);
    });
    this.initCompetences();
  }

  initCompetences() {
    this.competencesNumber = {};
    let i = 0;
    Object.keys(this.data.competences).forEach((competence) => {
      this.competencesNumber[competence] = 1 + (i++ % 6);
    });
  }

  competences(event, cle) {
    this.shadow.querySelector(".parcours>.focus")?.classList.remove("focus");
    event.currentTarget.classList.add("focus");
    let divCompetences = this.shadow.querySelector(".competences");

    this.shadow.querySelector(".competences").innerHTML = "";

    /* Création des compétences */
    let competencesBucket = [];
    Object.entries(this.data.parcours[cle].annees).forEach(
      ([annee, dataAnnee]) => {
        Object.entries(dataAnnee.competences).forEach(
          ([competence, niveauCle]) => {
            let numComp = this.competencesNumber[competence];
            let divCompetence = document.createElement("div");
            divCompetence.innerText = `${competence} ${niveauCle.niveau}`;
            divCompetence.style.gridRowStart = annee;
            divCompetence.style.gridColumnStart = competence.replaceAll(" ","_").replaceAll("'", "_");
            divCompetence.className = "comp" + numComp;
            divCompetence.dataset.competence = `${competence} ${niveauCle.niveau}`;
            divCompetence.addEventListener("click", (event) => {
              this.AC(event, competence, niveauCle.niveau, annee, numComp);
            });
            divCompetences.appendChild(divCompetence);

            competencesBucket.push(competence);
          }
        );
      }
    );

    /* Affectation de la taille des éléments */
    //divCompetences.style.setProperty("--competence-size", `calc(${100 / competencesBucket.length}% )`);
    let gridTemplate = "";
    Object.keys(this.data.competences).forEach((competence) => {
      if (competencesBucket.indexOf(competence) == -1) {
        gridTemplate += `[${competence.replaceAll(" ", "_").replaceAll("'", "_")}] 0`;
      } else {
        gridTemplate += `[${competence.replaceAll(" ", "_").replaceAll("'", "_")}] 1fr`;
      }
    });
    this.shadow.querySelector(".competences").style.gridTemplateColumns =
      gridTemplate;

    /* Réaffectation des focus */
    this.shadow.querySelectorAll(".AC").forEach((ac) => {
      this.shadow
        .querySelector(`[data-competence="${ac.dataset.competence}"]`)
        .classList.add("focus");
    });
  }

  AC(event, competence, niveau, annee, numComp) {
    event.currentTarget.classList.toggle("focus");
    if (
      this.shadow.querySelector(
        `.ACs [data-competence="${competence} ${niveau}"]`
      )
    ) {
      this.shadow
        .querySelector(`.ACs [data-competence="${competence} ${niveau}"]`)
        .remove();
    } else {
      let output = `
				<ul class=AC data-competence="${competence} ${niveau}">
					<h2 class=comp${numComp}>${competence} ${niveau}</h2>
			`;
      Object.entries(
        this.data.competences[competence].niveaux["BUT" + annee].app_critiques
      ).forEach(([num, contenu]) => {
        output += `<li><div class=comp${numComp}>${num}</div><div>${contenu.libelle}</div></li>`;
      });
      this.shadow.querySelector(".ACs").innerHTML += output + "</ul>";
    }
  }
}
customElements.define("ref-competences", ref_competences);
