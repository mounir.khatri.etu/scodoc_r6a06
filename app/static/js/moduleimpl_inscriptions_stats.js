/* Page Inscriptions aux modules et UE du semestre
 */

function change_ue_inscr(elt) {
  let url = "";
  if (elt.checked) {
    url = elt.dataset.url_inscr;
  } else {
    url = elt.dataset.url_desinscr;
  }
  $.post(url, {}, function (result) {
    sco_message("changement inscription UE enregistré");
  });
}

$(function () {
  $("table#but_ue_inscriptions").DataTable({
    paging: false,
    searching: true,
    info: false,
    autoWidth: false,
    fixedHeader: {
      header: true,
      footer: false,
    },
    orderCellsTop: true, // cellules ligne 1 pour tri
    aaSorting: [], // Prevent initial sorting
    oLanguage: {
      sSearch: "Chercher :",
    },
  });
});
