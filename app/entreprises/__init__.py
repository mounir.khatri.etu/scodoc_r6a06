"""entreprises.__init__
"""

from datetime import datetime
from flask import Blueprint
from app.scodoc import sco_etud
from app.auth.models import User
from app.models import Departement
import app.scodoc.sco_utils as scu

bp = Blueprint("entreprises", __name__)

LOGS_LEN = 5
SIRET_PROVISOIRE_START = "xx"


@bp.app_template_filter()
def format_prenom(s):
    return scu.format_prenom(s)


@bp.app_template_filter()
def format_nom(s):
    return scu.format_nom(s)


@bp.app_template_filter()
def get_nomcomplet_by_username(s):
    user = User.query.filter_by(user_name=s).first()
    return user.get_nomcomplet()


@bp.app_template_filter()
def get_nomcomplet_by_id(id):
    user = User.query.filter_by(id=id).first()
    return user.get_nomcomplet()


@bp.app_template_filter()
def get_dept_acronym(id):
    dept = Departement.query.filter_by(id=id).first()
    return dept.acronym


@bp.app_template_filter()
def get_civilité(civ):
    if civ == "H":
        return "Monsieur"
    else:
        return "Madame"


@bp.app_template_filter()
def check_taxe_now(taxes):
    for taxe in taxes:
        if taxe.annee == int(datetime.now().strftime("%Y")):
            return True
    return False


from app.entreprises import routes
