# -*- coding: utf-8 -*-

"""Test ScoDocSiteConfig (paramétrage général)


Utiliser comme: 
    pytest tests/unit/test_site_config.py

"""

from app.models.config import ScoDocSiteConfig, PersonalizedLink
from app.comp.bonus_spo import BonusIUTRennes1
from app.scodoc import sco_utils as scu


def test_scodoc_site_config(test_client):
    """Classe pour paramètres généraux"""
    cfg = ScoDocSiteConfig.get_dict()
    assert cfg == {}  # aucune valeur au départ
    try:
        ScoDocSiteConfig.set_bonus_sport_class("invalid")
        assert False  # la ligne precédente doit lancer une exception
    except NameError:
        pass
    bonus_sport = "bonus_iut_rennes1"
    ScoDocSiteConfig.set_bonus_sport_class(bonus_sport)
    cfg = ScoDocSiteConfig.get_dict()
    assert tuple(ScoDocSiteConfig.get_dict().values()) == (bonus_sport,)
    assert ScoDocSiteConfig.get_bonus_sport_class_name() == bonus_sport
    assert ScoDocSiteConfig.get_bonus_sport_class() == BonusIUTRennes1
    bonus_sport_class_names = ScoDocSiteConfig.get_bonus_sport_class_names()
    assert isinstance(bonus_sport_class_names, list)
    assert "" in bonus_sport_class_names
    assert bonus_sport in bonus_sport_class_names
    assert len(bonus_sport_class_names) > 5
    assert all([x == "" or x.startswith("bonus_") for x in bonus_sport_class_names])
    apo_dict = ScoDocSiteConfig.get_codes_apo_dict()
    assert isinstance(apo_dict, dict)
    assert "ABAN" in apo_dict
    assert "ADM" in apo_dict
    assert len(apo_dict) > 5
    ScoDocSiteConfig.set_code_apo("ADSUP", "AZERTY")
    assert ScoDocSiteConfig.get_codes_apo_dict()["ADSUP"] == "AZERTY"
    assert ScoDocSiteConfig.get_code_apo("ADSUP") == "AZERTY"
    assert ScoDocSiteConfig.get("azerty") == ""  # default empty string
    assert ScoDocSiteConfig.set("azerty", 99)
    assert ScoDocSiteConfig.get("azerty") == "99"  # converted to string
    assert "azerty" in ScoDocSiteConfig.get_dict()
    assert ScoDocSiteConfig.set("always_require_ine", 99)
    # Converti en bool car déclaré dans NAMES:
    assert ScoDocSiteConfig.get_dict()["always_require_ine"] == True
    assert ScoDocSiteConfig.get("always_require_ine") == True
    #
    assert (
        ScoDocSiteConfig.get_month_debut_annee_scolaire()
        == scu.MONTH_DEBUT_ANNEE_SCOLAIRE
    )
    # Links:
    assert ScoDocSiteConfig.get_perso_links() == []
    ScoDocSiteConfig.set_perso_links(
        [
            PersonalizedLink(title="lien 1", url="http://foo.bar/bar", with_args=True),
            PersonalizedLink(title="lien 1", url="http://foo.bar?x=1", with_args=True),
        ]
    )
    links = ScoDocSiteConfig.get_perso_links()
    assert links[0].get_url(params={"y": 2}) == "http://foo.bar/bar?y=2"
    assert links[1].get_url(params={"y": 2}) == "http://foo.bar?x=1&y=2"
